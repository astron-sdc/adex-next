# ADEX NEXT

This is the frontend application for the ASTRON Data Explorer (ADEX).

## Contributing

ADEX is a React application that uses the [Vite](https://vitejs.dev/) buildchain.
You can checkout this repository and run:

```bash
npm install
npm run dev
```

To perform code formatting and check for common errors you can run:
```bash
npm run format
npm run lint
```

## License

Apache License 2.0
