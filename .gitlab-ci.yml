include:
  - template: Jobs/SAST.gitlab-ci.yml

stages:
  - linting
  - test
  - build
  - integration
  - deploy

workflow:
  rules:
    # don't create a pipeline if it's a commit pipeline, on a branch and that branch has open merge requests.
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never

    # Tag docker images with latest
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      variables:
        DOCKER_IMAGE_TAG: "latest"

    # Tag docker images with a slug of the branch name
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
      variables:
        DOCKER_IMAGE_TAG: "$CI_COMMIT_REF_SLUG"

default:
  interruptible: true

.nodejs:
  image: node:lts-slim
  before_script:
    - node --version
    - npm ci

linting:
  extends: .nodejs
  stage: linting
  script:
    - npm run lint

sonarqube-check:
  stage: linting
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [ "" ]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner
  allow_failure: true
  only:
    - main
    - merge_requests

qodana:
  stage: linting
  image:
    name: jetbrains/qodana-js
    entrypoint: [ "" ]
  before_script:
    - node --version
    - npm ci
  variables:
    QODANA_TOKEN: $QODANA_TOKEN
  script:
    - qodana --save-report --results-dir=$CI_PROJECT_DIR/qodana --report-dir=$CI_PROJECT_DIR/qodana/report
  artifacts:
    paths:
      - qodana/report/
    expose_as: 'Qodana report'
  only:
    - main
    - merge_requests

sast:
  stage: test

unit_test:
  extends: .nodejs
  stage: test
  script:
    - npm run test

build_static_files:
  extends: .nodejs
  stage: build
  script:
    - npm run build
  artifacts:
    paths:
      - dist
    expire_in: "1w"

build_docker_image:
  stage: build
  image: docker:20-cli
  needs: [ build_static_files, unit_test ]
  services:
    - docker:20-dind
  before_script:
    - docker --version
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE:$DOCKER_IMAGE_TAG" .
    - docker push "$CI_REGISTRY_IMAGE:$DOCKER_IMAGE_TAG"

.deploy-step:
  stage: deploy
  image: docker:20-cli
  interruptible: false
  when: manual
  tags:
    - "sdc-dev"
  before_script:
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh && chmod 700 ~/.ssh
    - cp $SDC_KNOWN_HOSTS ~/.ssh/known_hosts
  script:
    - ssh $DEPLOY_USER@$DEPLOY_HOST "mkdir -p $SERVICE_DIR"
    - scp docker-compose.yml $DEPLOY_USER@$DEPLOY_HOST:$SERVICE_DIR/docker-compose.yml
    - |
      ssh $DEPLOY_USER@$DEPLOY_HOST "echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY &&\
      docker pull "$CI_REGISTRY_IMAGE:$DOCKER_IMAGE_TAG" &&\
      ADEX_VERSION=$DOCKER_IMAGE_TAG HOSTNAME=$HOSTNAME docker-compose -f $SERVICE_DIR/docker-compose.yml up -d --force-recreate"
    - echo "Application deployed"

deploy_to_test:
  extends: .deploy-step
  environment:
    name: sdc-dev
    url: https://sdc-dev.astron.nl/adex-next
  variables:
    SERVICE_DIR: /docker_compose/adex-next
    DEPLOY_HOST: dop814.astron.nl
    DEPLOY_USER: gitlab-deploy
    HOSTNAME: sdc-dev.astron.nl

deploy_to_production:
  extends: .deploy-step
  environment:
    name: sdc
    url: https://sdc.astron.nl/adex-next
  variables:
    SERVICE_DIR: /opt/dockercompose/adex-next
    DEPLOY_HOST: dop821.astron.nl
    DEPLOY_USER: sdco
    HOSTNAME: sdc.astron.nl
  only:
    - main
