import { Link, Outlet } from "react-router-dom";
import styles from "./layouts.module.css";

const Root = () => {
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <Link to="/">
          <h1>ASTRON Data Explorer</h1>
        </Link>
      </div>
      <div className={styles.horizontalContent}>
        <div className={styles.side}>
          <Link to="/skyview">
            <div className={styles.tabBtn}>Sky view</div>
          </Link>
          <Link to="/table">
            <div className={styles.tabBtn}>Table view</div>
          </Link>
        </div>
        <div className={styles.content}>
          <div className={styles.contentView}>
            <Outlet />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Root;
